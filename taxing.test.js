import { Taxing } from './taxing';

const sampleData = [
  {product: "Gummy Bears", price: 4.5},
  {product: "Soda pop 6-pack", price: 6.95},
  {product: "", price: 10},
  {product: "", price: 10},
  {product: "", price: 10},
  {product: "", price: 10},
  {product: "", price: 10},
  {product: "", price: 10},
  {product: "", price: 10},
  {product: "", price: 10},
  {product: "", price: 10},
]

if (Taxing.calculateOverallTax(sampleData) != 101.45) throw new Error('sum is wrong');


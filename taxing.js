const Taxing = {

  taxRates: {
    de: 0.19,
    be: 0.21,
    dk: 0.25,
    hu: 0.27,
    po: 0.23,
    uk: 0.2
  },

  calculateOverallTax: (country, itemList) =>
    itemList.reduce(Taxing.aggregator(Taxing.taxRates[country]), 0),

  aggregator: taxRate => (acc, item) =>
    acc + item.price * taxRate

}

// For comparison, the original, imperative version
const calculateOverallTax = (itemList) => {
  const numItems = itemList.length;
  let totalTax = 0;
  for (let i=0; i < numItems; i++) {
    totalTax += itemList[i].price * Taxing.vat;
  }
  return totalTax;
}
